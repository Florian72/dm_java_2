import java.util.Collections;
import java.util.List;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Map;
import java.util.HashMap;
import java.util.Comparator;

public class BibDM{

    /**
     * Ajoute deux entiers
     * @param a le premier entier à ajouter
     * @param b le deuxieme entier à ajouter
     * @return la somme des deux entiers
     */
    public static Integer plus(Integer a, Integer b){
        return a+b;
    }



    /**
     * Renvoie la valeur du plus petit élément d'une liste d'entiers
     * VOUS DEVEZ LA CODER SANS UTILISER COLLECTIONS.MIN (i.e. vous devez le faire avec un for)
     * @param liste
     * @return le plus petit élément de liste
     */



    public static Integer min(List<Integer> liste){
        if (liste.size()<1)
            return null;
        Integer min = liste.get(0);
        for (int i = 1; i<liste.size(); i++){
            if (liste.get(i) < min){
                min = liste.get(i);
            }
        }
        return min;
    }



    /**
     * Teste si tous les élements d'une liste sont plus petits qu'une valeure donnée
     * @param valeur 
     * @param liste
     * @return true si tous les elements de liste sont plus grands que valeur.
     */



    public static<T extends Comparable<? super T>> boolean plusPetitQueTous(T valeur ,List<T> liste){
        if (liste.size()<1)
            return true;
        Collections.sort(liste);
        int compare = valeur.compareTo(liste.get(0));
        if (compare>=0)
            return false;
        return true;
    }



    /**
     * Intersection de deux listes données par ordre croissant.
     * @param liste1 une liste triée
     * @param liste2 une liste triée
     * @return une liste triée avec les éléments communs à liste1 et liste2
     */


    public static <T extends Comparable<? super T>> List<T> intersection(List<T> liste1, List<T> liste2){
        List<T> listeF = new ArrayList<>();
        int comp = 0;
        int i = 0;
        int j = 0;
        while ( i < liste1.size() ){
            if (liste1.get(i).equals(liste2.get(j)) && !listeF.contains(liste1.get(i))){
                listeF.add(liste1.get(i));
                i++;
                j++;
            }
            else{
                comp = liste1.get(i).compareTo(liste2.get(j));
                if (comp > 0)
                    j++;
                else
                    i++;
            }
        } 
        return listeF;
    }



    /**
     * Découpe un texte pour obtenir la liste des mots le composant. texte ne contient que des lettres de l'alphabet et des espaces.
     * @param texte une chaine de caractères
     * @return une liste de mots, correspondant aux mots de texte.
     */



    public static List<String> decoupe(String texte){
        List<String> phrase = new ArrayList<String>();
        String mot = "";
        for (int i=0; i<texte.length(); i++){
            if (!String.valueOf(texte.charAt(i)).equals(" "))
                mot+=String.valueOf(texte.charAt(i));
            else if (mot != ""){
                phrase.add(mot);
                mot = "";
            }
        }
        if (mot != "")
            phrase.add(mot);
        return phrase;
    }



    /**
     * Renvoie le mot le plus présent dans un texte.
     * @param texte une chaine de caractères
     * @return le mot le plus présent dans le texte. En cas d'égalité, renvoyer le plus petit dans l'ordre alphabétique
     */



    public static String motMajoritaire(String texte){
        if (texte == "")
            return null;
        List<String> listeT = new ArrayList<String>();
        listeT = decoupe(texte);
        //System.out.println(listeT);
        Map<String,Integer> texteMajo = new HashMap<String,Integer>();
        for (String mot : listeT){
            if (texteMajo.containsKey(mot))
                texteMajo.replace(mot,texteMajo.get(mot)+1);
            else{
                texteMajo.put(mot,1);
            }
        }
        int max = 0;
        String maxStr = "";
        for (String mot : texteMajo.keySet()){
            if (texteMajo.get(mot)>=max){
                if (texteMajo.get(mot)==max){
                    int co = maxStr.compareTo(mot);
                    if (co > 0){
                        max = texteMajo.get(mot);
                        maxStr = mot;
                    }
                }
                else{
                    max = texteMajo.get(mot);
                    maxStr = mot;
                }
                
            }
        }
        return maxStr;
    }



    /**
     * Permet de tester si une chaine est bien parenthesée
     * @param chaine une chaine de caractères composée de ( et de )
     * @return true si la chaine est bien parentjèsée et faux sinon. Par exemple ()) est mal parenthèsée et (())() est bien parenthèsée.
     */



    public static boolean bienParenthesee(String chaine){
        int bienP = 0;
        if (chaine == "")
            return true;
        else if (!(String.valueOf(chaine.charAt(0)).equals("(") && String.valueOf(chaine.charAt(chaine.length()-1)).equals(")")))
            return false;
        else{ 
            for (int i = 0; i<chaine.length(); i++){
                if (String.valueOf(chaine.charAt(i)).equals("("))
                    bienP+=1;
                else
                    bienP-=1;
            }
        }
        return bienP==0;
    }
    


    /**
     * Permet de tester si une chaine est bien parenthesée
     * @param chaine une chaine de caractères composée de (, de  ), de [ et de ]
     * @return true si la chaine est bien parentjèsée et faux sinon. Par exemple ([)] est mal parenthèsée alors ue ([]) est bien parenthèsée.
     */



     // ( suivi de ( ou ) ou [
     // [ suivi de [ ou ] ou (
    public static boolean bienParentheseeCrochets(String chaine){
        int bienP = 0;
        int bienC = 0;
        if (chaine == "")
            return true;
        else{
            if ((String.valueOf(chaine.charAt(0)).equals(")") || String.valueOf(chaine.charAt(0)).equals("]")) || (String.valueOf(chaine.charAt(chaine.length()-1)).equals("(") || String.valueOf(chaine.charAt(chaine.length()-1)).equals("[")) )
                return false;
            else{
                if (String.valueOf(chaine.charAt(0)).equals("("))
                    bienP+=1;
                else
                    bienC+=1;
                for (int i = 1; i<chaine.length(); i++){
                    if ((String.valueOf(chaine.charAt(i-1)).equals("(") && String.valueOf(chaine.charAt(i)).equals("]")) || (String.valueOf(chaine.charAt(i-1)).equals("[") && String.valueOf(chaine.charAt(i)).equals(")")))
                        return false;
                    else{
                        if (String.valueOf(chaine.charAt(i)).equals("("))
                            bienP+=1;
                        else{
                            if (String.valueOf(chaine.charAt(i)).equals(")"))
                            bienP-=1;
                            else{
                                if (String.valueOf(chaine.charAt(i)).equals("["))
                                    bienC+=1;
                                else
                                    bienC-=1;
                            }
                        }
                    }
                }
            }
        }
        return bienP == 0 && bienC == 0;
    }



    /**
     * Recherche par dichtomie d'un élément dans une liste triée
     * @param liste, une liste triée d'entiers
     * @param valeur un entier
     * @return true si l'entier appartient à la liste.
     */



    public static boolean rechercheDichotomique(List<Integer> liste, Integer valeur){
        if (liste.size() == 0)
            return false;
        int bas = 0;
        int haut =  liste.size()-1;
        while(bas < haut){
            int milieu = (bas+haut)/2;
            if (liste.get(milieu) < valeur)
                bas = milieu + 1;
            else
                haut = milieu;
        }
        return bas < liste.size() && valeur == liste.get(bas);
    }
}
